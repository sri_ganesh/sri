package org.altimtrik.sample.boot.zuul;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableEurekaClient
@EnableZuulProxy
public class ZuulApp {

	public static void main(String[] args) {
		System.out.println("SRI GANESH");
		SpringApplication.run(ZuulApp.class, args);
	}
}