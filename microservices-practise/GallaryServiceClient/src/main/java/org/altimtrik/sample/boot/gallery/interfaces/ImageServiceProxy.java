package org.altimtrik.sample.boot.gallery.interfaces;

import java.util.Map;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("image-service")
public interface ImageServiceProxy {

	@GetMapping("/SRI")
	public Map<String, String> getImages();
}
