package org.altimtrik.sample.boot.gallery;

import java.util.Map;

import org.altimtrik.sample.boot.gallery.interfaces.ImageServiceProxy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@RestController
@Configuration
@EnableFeignClients
@EnableDiscoveryClient
public class GalleryApp {

	@Autowired
	private ImageServiceProxy imageServiceProxy;

	@Bean
	@LoadBalanced
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/get/{id}")
	public String getGallery(@PathVariable String id) {
		Map<String, String> map = restTemplate.getForObject("http://image-service/SRI", Map.class);
		return map.get(id);
	}

	@GetMapping("/fetch/{id}")
	public String getImageById(@PathVariable String id) {
		return imageServiceProxy.getImages().get(id);
	}

	public static void main(String[] args) {
		System.out.println("SRI GANESH");
		SpringApplication.run(GalleryApp.class, args);
	}
}
