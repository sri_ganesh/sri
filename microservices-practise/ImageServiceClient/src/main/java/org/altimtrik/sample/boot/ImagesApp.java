package org.altimtrik.sample.boot;

import java.util.HashMap;
import java.util.Map;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
public class ImagesApp {
	public static void main(String[] args) {
		System.out.println("SRI GANESH");
		SpringApplication.run(ImagesApp.class, args);
	}

	@GetMapping("/SRI")
	public Map<String, String> fetchImages() {
		Map<String, String> map = new HashMap<>();
		map.put("1", "SRI GANESH");
		map.put("2", "JAY TULJA BHAVANI MATA");
		map.put("3", "HAR HAR MAHADEV");
		map.put("4", "OM SRI GURUDEV DATT");
		map.put("5", "JAY SRI RAM JAY HANUMAAN");
		map.put("6", "JAY SHANIDEV");
		return map;
	}
}
